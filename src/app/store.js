import { createStore, combineReducers, applyMiddleware } from "redux";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";
import authReducer from "../features/login/authReducer";
import authgetProfileReducer from '../features/login/authgetProfileReducer';
import authLogOutReducer from '../features/login/authLogOutReducer';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import { persistStore, persistReducer } from "redux-persist";

const persistConfig = {
  key: "root",
  storage,
};

const rootReducer = combineReducers({
  auth:authReducer,
  profile:authgetProfileReducer,
  logout:authLogOutReducer,
  routing: routerReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default ()=> {
  let store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(logger, thunk))
  );
  let persistor = persistStore(store);
  return { store, persistor };
} 


// export const history = syncHistoryWithStore(browserHistory, store)
