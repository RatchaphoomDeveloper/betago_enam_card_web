import {
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  RESET_STORE,
} from "./authType";

const InitialState = {
  loading: false,
  loaded: false,
  data: [],
  error: "",
};
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case GET_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        data: [],
        error: "",
      };
    case GET_PROFILE_SUCCESS:
      console.log("state", state);
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload,
        error: "",
      };
    case RESET_STORE:
      return {
        loading: false,
        loaded: false,
        data: [],
        error: "",
      };
    default:
      return state;
  }
};

export default reducer;
