import {
  GET_PROFILE_FAILURE,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  RESET_STORE,
  SET_LOGIN_FAILURE,
  SET_LOGIN_REQUEST,
  SET_LOGIN_SUCCESS,
  SET_LOGOUT_FAILURE,
  SET_LOGOUT_REQUEST,
  SET_LOGOUT_SUCCESS,
} from "./authType";
import axios from "axios";
import history from "../../app/history";
import Swal from "sweetalert2";

// Login //
export const setLoginRequest = (payload) => ({
  type: SET_LOGIN_REQUEST,
  payload,
});

export const setLoginSuccess = (payload) => ({
  type: SET_LOGIN_SUCCESS,
  payload,
});

export const setLoginFailure = (payload) => ({
  type: SET_LOGIN_FAILURE,
  payload,
});

export const setLoginReset = (payload) => ({
  type: RESET_STORE,
  payload,
});

// Profile //
export const getLoginProfileRequest = (payload) => ({
  type: GET_PROFILE_REQUEST,
  payload,
});

export const getLoginProfileSuccess = (payload) => ({
  type: GET_PROFILE_SUCCESS,
  payload,
});

export const getLoginProfileFailure = (payload) => ({
  type: GET_PROFILE_FAILURE,
  payload,
});

export const getLoginProfileReset = (payload) => ({
  type: RESET_STORE,
  payload,
});

// Logout //
export const setLogOutRequest = (payload) => ({
  type: SET_LOGOUT_REQUEST,
  payload,
});

export const setLogOutSuccess = (payload) => ({
  type: SET_LOGOUT_SUCCESS,
  payload,
});

export const setLogOutFailure = (payload) => ({
  type: SET_LOGOUT_FAILURE,
  payload,
});
export const setLogOutReset = () => ({
  type: RESET_STORE,
});

export const setLogin = (data) => {
  const statusLogin = false;
  return (dispatch) => {
    dispatch(setLoginRequest);
    try {
      axios
        .get(
          `Account/Login?emplid=${data.emplid}&pinno=${data.pinno}&language=${data.language}`,
          {}
        )
        .then((res) => {
          if (res.data.data.message.returncode === 0) {
            dispatch(
              setLoginSuccess({
                ...data,
                token: res.data.token,
                status: 200,
              })
            );
            
          }if (res.data.status === false) {
             Swal.fire({
               title: res.data.message,
               icon: "error",
             });
          }
          
        });
    } catch (error) {
     
      console.log();
    }
    return "Success"
  };
};

export const getProfile = (data) => {
  return (dispatch) => {
    dispatch(getLoginProfileRequest);
    try {
      axios
        .get(`Account/GetUserProfile?employeeId=${data.emplid}&language=th`, {
          headers: {
            Authorization:
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiMDA1NTIxNCIsImp0aSI6Ijc2ZTViMGViLTBlNjMtNGRmYS04MmQ3LTUwZjMxMmEyM2ZmYyIsIm5iZiI6MTYxMTQyMjg1NSwiZXhwIjoxNjEyNzE4ODU1LCJpc3MiOiJNZW1vUHJvamVjdCIsImF1ZCI6IkFueW9uZSJ9.fI-w7Nmhe51gXOSXiBsgbeOgcYZ8cHxtUuyKuf6jGtE",
          },
        })
        .then((res) => {
          console.log(res.data);
          if (res.data.message === "Success") {
            dispatch(getLoginProfileSuccess(res.data.data));
          }
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const setLogOut = (data) => {
  return (dispatch) => {
    dispatch(setLogOutRequest);
    try {
      axios
        .get(`Account/Logout?emplid=${data.emplid}`, {
          headers: {
            Authorization:
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiMDA1NTIxNCIsImp0aSI6Ijc2ZTViMGViLTBlNjMtNGRmYS04MmQ3LTUwZjMxMmEyM2ZmYyIsIm5iZiI6MTYxMTQyMjg1NSwiZXhwIjoxNjEyNzE4ODU1LCJpc3MiOiJNZW1vUHJvamVjdCIsImF1ZCI6IkFueW9uZSJ9.fI-w7Nmhe51gXOSXiBsgbeOgcYZ8cHxtUuyKuf6jGtE",
          },
        })
        .then((res) => {
          console.log(res.data);
          if (res.data.status === true) {
            dispatch(setLogOutSuccess(res.data.status));
            dispatch(setLoginReset());
            dispatch(getLoginProfileReset());
          }
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};
