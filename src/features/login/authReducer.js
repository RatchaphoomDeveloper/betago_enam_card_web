import { RESET_STORE, SET_LOGIN_REQUEST, SET_LOGIN_SUCCESS } from "./authType";

const InitialState = {
  loading: false,
  loaded: false,
  data: [],
  error: "",
};
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case SET_LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        data: [],
        error: "",
      };
    case SET_LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload,
        error: "",
      };
    case RESET_STORE:
      return {
        loading: false,
        loaded: false,
        data: [],
        error: "",
      };
    default:
      return state;
  }
};

export default reducer;
