import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, WingBlank } from "antd-mobile";
import "../../scss/Detailpage.scss";
// import "../../scss/Loginpage.scss";
import { Col, Row, Skeleton, Typography } from "antd";
import { StaticImage } from "../../function/StaticImage";
const { Title, Text } = Typography;

const IDcard = () => {
  const profile = useSelector((state) => state.profile);
  const [subLastName, setsubLastName] = useState("");
  const authState = useSelector((state) => state.auth);
  useEffect(() => {
    console.log(profile);
  }, []);

  const subNameFunction = (data) => {
    let arr = "";
    var str = "";
    if (data) {
      str = data;
      arr = str.split(" ");
    }

    return `${arr[0]} ${arr[1]}`;
  };

  return (
    <div>
      {profile?.loaded === true && (
        <WingBlank >
          <WingBlank size="lg" style={{ marginTop: "1pc" }} />
          <Card style={{height:"90vh"}}>
            <Card.Body >
              <Row wrap={false}>
                <Col flex="none">
                  <section
                    className="section-container-ID"
                    style={{ marginTop: "2pc",paddingLeft:"1pc" }}
                  >
                    <img
                      src={StaticImage("/images/Group3.png")}
                      className="center2"
                      style={{backgroundPosition:"center"}}
                    />
                  </section>
                </Col>
                <Col flex="auto">
                  <section style={{ marginTop: "2pc" }}>
                    <img
                      src={profile.loaded === true && profile.data?.photo}
                      style={{ float: "right", border: "1px solid #EAEDED" }}
                      width="140%"
                    />
                  </section>
                </Col>
              </Row>
              <section>
                <Row>
                  {profile.loaded === true && (
                    <Col span={24} style={{ textAlign: "right",paddingTop:"1pc" }}>
                      <Text className="card-text-name">
                        {`${
                          profile.loaded === true &&
                          profile.data?.firstname_card_en
                        } ` +
                          subNameFunction(
                            profile.loaded === true &&
                              profile?.data?.lastname_card_en
                          )}
                      </Text>
                    </Col>
                  )}
                </Row>
                <Row>
                  <Col span={24} style={{ textAlign: "right" }}>
                    <Text className="card-text-name-th">
                      {profile.loaded === true &&
                        `${profile.data.firstname_card_th} ${profile.data.lastname_card_th}`}
                    </Text>
                  </Col>
                </Row>
                <Row>
                  <Col span={24} style={{ textAlign: "right" }}>
                    <Text className="card-text-name-th">{`${
                      authState.loaded === true && authState.data.emplid
                    }`}</Text>
                  </Col>
                </Row>
              </section>
              <section>
                {/* <div className="barcode-container">
                <img
                  src={StaticImage("/images/Barcode.png")}
                  className="barcode-center"
                />
              </div> */}
                <div className="hero-image" style={{position:"absolute",bottom:"0",left:"0",right:"0"}}>
                  <div className="hero-text">
                    <img
                      src={`data:image/jpeg;base64,${profile.data.barcode}`}
                      style={{ backgroundPosition: "center" }}
                      width="200"
                      height="50"
                    />
                  </div>
                </div>
              </section>
            </Card.Body>
          </Card>
          <WingBlank size="lg" />
        </WingBlank>
      )}
    </div>
  );
};

export default IDcard;
