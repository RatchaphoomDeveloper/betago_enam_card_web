import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, WingBlank } from "antd-mobile";
import { StaticImage } from "../../function/StaticImage";
import "../../scss/Detailpage.scss";
import { Col, Row, Typography } from "antd";
const { Title, Text } = Typography;
const QRcode = () => {
  const profile = useSelector((state) => state.profile.data);
  useEffect(() => {
    console.log(profile);
  }, []);
  return (
    <div>
      <WingBlank style={{ marginTop: "1pc" }}>
        <Card style={{ height: "80vh" }}>
          {" "}
          <section className="section-container-qrcode">
            <img src={StaticImage("/images/Group3.png")} className="center" />
          </section>
          <section style={{ paddingTop: "2pc" }}>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text className="card-text-name-th">
                  {profile &&
                    `${profile.firstname_card_th} ${profile.lastname_card_th}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text className="card-text-name-th-qr">
                  {profile &&
                    `${profile.firstname_card_en} ${profile.lastname_card_en}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text className="card-text-name-th-qr">
                  {profile && `${profile.position_eng}`}
                </Text>
              </Col>
            </Row>
          </section>
          <section  className="centered">
            <div className="hero-image2">
              <div className="hero-text">
                <img
                  src={`data:image/jpeg;base64,${profile && profile.vcard}`}
                  style={{ backgroundPosition: "center" }}
                  width="100%"
                  
                />
              </div>
            </div>
          </section>
          <div
                className="hero-image2"
                style={{
                  position: "absolute",
                  bottom: "0",
                  left: "0",
                  right: "0",
                  marginBottom:"2pc"
                }}
              >
                <div className="hero-text">
                <img src={StaticImage("/images/LoginGroupe2.png")} />
                </div>
              </div>{" "}
        </Card>
      </WingBlank>
    </div>
  );
};

export default QRcode;
