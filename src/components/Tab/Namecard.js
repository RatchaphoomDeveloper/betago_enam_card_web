import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Card, WingBlank, WhiteSpace } from "antd-mobile";
import { StaticImage } from "../../function/StaticImage";
import { Col, Row, Skeleton, Typography } from "antd";
import Qrcodeicon from "../icons/Qrcodeicon";
const { Title, Text } = Typography;

const Namecard = () => {
  const profile = useSelector((state) => state.profile.data);
  const [subLastName, setsubLastName] = useState("");
  useEffect(() => {
    console.log(profile);
  }, []);

  return (
    <div>
      <WingBlank style={{ marginTop: "1pc" }}>
        <Card  style={{height:"80vh"}}>
          {" "}
          <section className="section-container-qrcode">
            <img src={StaticImage("/images/Group3.png")} className="center" />
          </section>
          <section style={{ paddingTop: "2pc" }}>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text className="card-text-name-th">
                  {profile &&
                    `${profile.firstname_card_th} ${profile.lastname_card_th}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text className="card-text-name-th-qr">
                  {profile &&
                    `${profile.firstname_card_en} ${profile.lastname_card_en}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text strong className="card-text-name-th-sub2">
                  T.{" "}
                </Text>
                <Text className="card-text-name-th-sub2">
                  {profile && `${profile.phone_mobile}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24} style={{ textAlign: "center" }}>
                <Text strong className="card-text-name-th-sub2">
                  M.{" "}
                </Text>
                <Text className="card-text-name-th-sub2">
                  {profile && `${profile.phone_mobile}`}{" "}
                </Text>
                <Text
                  strong
                  className="card-text-name-th-sub2"
                  style={{ paddingLeft: "0.2pc" }}
                >
                  {" "}
                  E.
                </Text>
                <Text className="card-text-name-th-sub2">
                  {profile && `${profile.email}`}
                </Text>
              </Col>
            </Row>
          </section>
          <section style={{ paddingTop: "4pc", paddingBottom: "3.5pc" }}>
            <div className="hero-image2">
              <div className="hero-text">
                <img
                  src={profile && profile.photo}
                  style={{backgroundPosition:"center",border: "1px solid #EAEDED" }}
                  width="70%"
                />
              </div>
            </div>
          </section>
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              <Text className="card-text-name-th-qr">BETAGRO GROUP</Text>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              <Text className="card-text-name-th-qr">
                BETAGRO Tower (North Park)
              </Text>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              <Text className="card-text-name-th-qr">
                บริษัท เบทาโกร จำกัด(มหาชน)
              </Text>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              <Text className="card-text-name-th-qr">
                323 ถนนวิภาวดีรังสิต หมูุ่ 6 แขวงทุ่งสองห้อง
              </Text>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              <Text className="card-text-name-th-qr">
                เขตุหลักสี่ กรุงเทพมหานคร 10210
              </Text>
            </Col>
          </Row>
        </Card>
      </WingBlank>
    </div>
  );
};

export default Namecard;
