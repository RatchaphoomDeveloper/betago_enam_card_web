import React from "react";
import "./App.scss";
import Navigation from "./routes";
import "sweetalert2/src/sweetalert2.scss";
function App() {
  console.log(process.env);
  return (
    <div>
      <Navigation />
    </div>
  );
}

export default App;
