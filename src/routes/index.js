import React from 'react'
import {BrowserRouter as Routers,Switch,Route,Redirect,Router } from 'react-router-dom'
import Detailpage from '../pages/Detailpage';
import Loginpage from '../pages/Loginpage'
import history from '../app/store'

const Navigation = () => {
    return (
      <div>
        <Routers history={history}>
          <Switch>
            <Route exact path="/" component={Loginpage} />
            <Route  path="/AcesCard" component={Detailpage} />
          </Switch>
        </Routers>
      </div>
    );
}

export default Navigation
