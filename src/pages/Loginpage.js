import React, { useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { StaticImage } from "../function/StaticImage";
import { Input, Form, Checkbox, Button, Row, Col, Typography } from "antd";
import "../scss/Loginpage.scss";
import { getProfile, setLogin } from "../features/login/authAction";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";

const { Text, Link } = Typography;

const tailLayout = {
  wrapperCol: { span: 16 },
};

const Loginpage = () => {
  const history = useHistory();
  const [form] = Form.useForm();
  const { register, handleSubmit, watch, errors } = useForm();
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile);
  const authState = useSelector((state) => state.auth);
  const isInitialMount = true;

  const onSubmit = (data) => {
    console.log("dsfsdfsdfsdf", data);
  };
  const onFinish = (values) => {
    try {
      dispatch(
        setLogin({
          emplid: values.username,
          pinno: values.password,
          language: "th",
        })
      );
      dispatch(getProfile({ emplid: values.username }));
    } catch (error) {
      alert("ไม่สามารถเข้าสู่ระบบได้");
    }

    //
  };

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      console.log("TEST",authState)
      if (authState.loaded === true && profile.loaded === true) {
        Swal.fire({
          title: `Welcome ${profile && profile.data.firstname_card_en}`,
          icon: "success",
        });
        history.push("/AcesCard");
      }
      // Your useEffect code here to be run on update
    }
  });

  return (
    <div>
      <section className="section-container">
        <img src={StaticImage("/images/Group3.png")} className="center" />
      </section>
      <section className="section2-login-container">
        <label>เข้าสู่ระบบ</label>
      </section>
      <section style={{ margin: "16px 24px 16px 16px" }}>
        <Form
          initialValues={{ remember: true }}
          form={form}
          onFinish={onFinish}
          onSubmit={handleSubmit(onSubmit)}
        >
          <Form.Item
            name="username"
            rules={[
              { required: true, message: "กรุณากรอกชื่อ หรือ รหัสบัตรประชาชน" },
            ]}
          >
            <Input placeholder="รหัสพนักงาน" className="text-line" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "กรุณากรอกรหัสผ่าน" }]}
          >
            <Input.Password placeholder="รหัสผ่าน" className="text-line" />
          </Form.Item>
          <section className="section3-login-remember-container">
            <Form.Item {...tailLayout} name="remember" valuePropName="checked">
              <Checkbox>
                ฉันได้อ่านและยอมรับ
                <Text underline style={{ color: "#84BD00" }}>
                  ข้อตกลงและเงื่อนไข
                </Text>
                และ
                <Text underline style={{ color: "#84BD00" }}>
                  นโยบายความเป็นส่วนตัว
                </Text>
              </Checkbox>
            </Form.Item>
          </section>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              size={"large"}
              block
              className="login-bottom"
              style={{color:"#ffff"}}
            >
              เข้าสู่ระบบ
            </Button>
          </Form.Item>
        </Form>
      </section>

      <section className="under-logo-container">
        <Row justify={"end"}>
          <Col>
            {" "}
            <img src={StaticImage("/images/LoginGroupe2.png")} />
          </Col>
        </Row>
      </section>
    </div>
  );
};

export default Loginpage;
