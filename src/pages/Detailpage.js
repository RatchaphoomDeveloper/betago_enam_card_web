import React, { useEffect, useState,useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Layout } from "antd";
import { Tabs, Badge, NavBar, Icon } from "antd-mobile";
import "../scss/Detailpage.scss";
import IDcard from "../components/Tab/IDcard";
import QRcode from "../components/Tab/QRcode";
import Namecard from "../components/Tab/Namecard";
import { UserOutlined } from "@ant-design/icons";
import { setLogOut } from "../features/login/authAction";
const { Header } = Layout;
const tabs2 = [
  { title: "ID Card", sub: "1" },
  { title: "QR Code", sub: "2" },
  { title: "Name Card", sub: "3" },
];

const Detailpage = () => {
  const authState = useSelector((state) => state.auth);
  const profileState = useSelector((state) => state.profile);
  const logoutState = useSelector((state) => state.logout);
  const history = useHistory();
  const dispatch = useDispatch();
  const isInitialMount = true;
  const logOut = () => {
    dispatch(setLogOut(authState.data.emplid));
  };
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      console.log(authState.loaded)
      if (authState.loaded === false) {
        history.push("/");
      }
      // Your useEffect code here to be run on update
    }
  });
  return (
    <div>
      <Layout style={{ backgroundColor: "#D3D5D2;" }}>
        <Header
          className="section-header-container"
          style={{ height: "42px" }}
        ></Header>

        <NavBar
          mode="light"
          onLeftClick={() => console.log("onLeftClick")}
          rightContent={[<UserOutlined onClick={logOut} />]}
        >
          ID Card
        </NavBar>
        <Tabs
          tabs={tabs2}
          initialPage={0}
          onChange={(tab, index) => {
            console.log("onChange", index, tab);
          }}
          onTabClick={(tab, index) => {
            console.log("onTabClick", index, tab);
          }}
        >
          {profileState.loaded === true && (
            <div>
              <IDcard />
            </div>
          )}

          {profileState.loaded === true && (
            <div>
              <QRcode />
            </div>
          )}

          {profileState.loaded === true && (
            <div>
              <Namecard />
            </div>
          )}
        </Tabs>
      </Layout>
    </div>
  );
};

export default Detailpage;
