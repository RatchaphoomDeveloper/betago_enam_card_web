export const StaticImage = (filename) => {
  return (
    "http://" + window.location.hostname + ":" + window.location.port + filename
  );
};
